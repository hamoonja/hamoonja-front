const PRICE_PER_ROOM_DAY = 330;
const MIN_PRICE = 20000;

const discount = (price) => price;

const calculatePrice = (capacity, expDays) => {
  const beforeDiscount = Math.max(
    capacity * expDays * PRICE_PER_ROOM_DAY,
    MIN_PRICE
  );
  return discount(beforeDiscount);
};

export default calculatePrice;
