import calculatePrice from './calculate-price';
import polishPriceString from './polish-price-string';

const buildPriceString = (cap, exp) =>
  polishPriceString(calculatePrice(cap, exp));

export default buildPriceString;
