const { nanoid } = require('nanoid')
const mongoIDLength = 12

module.exports = {
    randStr: (size) => size && nanoid(size) || nanoid(),
    mongoID: () => nanoid(mongoIDLength)
}