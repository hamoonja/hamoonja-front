import convertNumbersToRoman from './convert-numbers-to-roman';

const isPhoneNumber = (text) => {
  const pattern = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/g;
  return pattern.test(text);
};

const parseMobileNumber = (text) => {
  try {
    const romanText = convertNumbersToRoman(text);
    if (!isPhoneNumber(romanText)) return { roman: romanText, mobile: null };
    const normalizedText = romanText.replace(/\D/g, '').replace(/^0+/, '');
    const minLen = /^98/g.test(normalizedText) ? 12 : 10;
    if (normalizedText.length === minLen)
      return { roman: romanText, mobile: romanText };
    return { roman: romanText };
  } catch (error) {
    return { roman: '' };
  }
};

export default parseMobileNumber;
