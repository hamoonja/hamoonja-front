const persianDigitLookup = {
  0: String.fromCharCode(1776),
  1: String.fromCharCode(1777),
  2: String.fromCharCode(1778),
  3: String.fromCharCode(1779),
  4: String.fromCharCode(1780),
  5: String.fromCharCode(1781),
  6: String.fromCharCode(1782),
  7: String.fromCharCode(1783),
  8: String.fromCharCode(1784),
  9: String.fromCharCode(1785),
};

const convertNumbersToPersian = (number) =>
  number
    .toString()
    .split('')
    .reduce((acc, c) => `${acc}${persianDigitLookup[c] || c}`, '');

export default convertNumbersToPersian;
