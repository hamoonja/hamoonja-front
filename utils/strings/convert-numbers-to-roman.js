const romanDigitLookup = {};
romanDigitLookup[String.fromCharCode(1776)] = '0';
romanDigitLookup[String.fromCharCode(1777)] = '1';
romanDigitLookup[String.fromCharCode(1778)] = '2';
romanDigitLookup[String.fromCharCode(1779)] = '3';
romanDigitLookup[String.fromCharCode(1780)] = '4';
romanDigitLookup[String.fromCharCode(1781)] = '5';
romanDigitLookup[String.fromCharCode(1782)] = '6';
romanDigitLookup[String.fromCharCode(1783)] = '7';
romanDigitLookup[String.fromCharCode(1784)] = '8';
romanDigitLookup[String.fromCharCode(1785)] = '9';
romanDigitLookup[String.fromCharCode(1632)] = '0';
romanDigitLookup[String.fromCharCode(1633)] = '1';
romanDigitLookup[String.fromCharCode(1634)] = '2';
romanDigitLookup[String.fromCharCode(1635)] = '3';
romanDigitLookup[String.fromCharCode(1636)] = '4';
romanDigitLookup[String.fromCharCode(1637)] = '5';
romanDigitLookup[String.fromCharCode(1638)] = '6';
romanDigitLookup[String.fromCharCode(1639)] = '7';
romanDigitLookup[String.fromCharCode(1640)] = '8';
romanDigitLookup[String.fromCharCode(1641)] = '9';

const convertNumbersToRoman = (text) =>
  text.split('').reduce((acc, c) => `${acc}${romanDigitLookup[c] || c}`, '');

export default convertNumbersToRoman;
