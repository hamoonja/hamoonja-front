const appName = 'hamoonja-front';
const deployToAddress = '/home/easyadmin/code/js/hamoonja-front/';
const gitRepo = 'https://gitlab.com/hamoonja/hamoonja-front.git';
const serverSSHaddress = 'easyadmin@94.182.190.182';
const ecosystemFileName = 'ecosystem.config.js';

function buildEcosystemConfigFile(fileName, releasePath) {
  const fs = require('fs');

  const ecosystem = `
    module.exports = {
      apps : [{
        name: '${appName}',
        script: 'cd ${releasePath} && npm run start',
        watch: true,
        autorestart: true,
        restart_delay: 1000,
        env: {
          NODE_ENV: 'development',
          PORT: 3999,
        },
        env_production: {
          NODE_ENV: 'production',
          PORT: 4000,
        }
      }],
    };
  `;
  fs.writeFileSync(fileName, ecosystem);
}

module.exports = (shipit) => {
  require('shipit-deploy')(shipit);
  // require('shipit-shared')(shipit);
  const path = require('path');

  shipit.initConfig({
    default: {
      deployTo: deployToAddress,
      repositoryUrl: gitRepo,
      keepReleases: 5,
    },
    production: {
      servers: serverSSHaddress,
    },
  });
  const ecosystemFilePath = path.join(
    shipit.config.deployTo,
    ecosystemFileName
  );

  shipit.on('updated', () => {
    shipit.start('copy-config');
  });

  shipit.on('config-ready', () => {
    shipit.start('prepare-env');
  });

  // shipit.on('env-ready', () => {
  //   shipit.start('build-next-app');
  // });

  // shipit.on('built', () => {
  //   shipit.start('deploy-new-release');
  // });

  shipit.blTask('copy-config', async () => {
    try {
      buildEcosystemConfigFile(ecosystemFileName, shipit.releasePath);
      await shipit.copyToRemote(ecosystemFileName, ecosystemFilePath);
      shipit.emit('config-ready');
    } catch (error) {
      console.log('error happened ib creating ecosystem config file');
    }
  });

  shipit.blTask('prepare-env', async () => {
    await shipit.copyToRemote('.env.production', shipit.releasePath);
    shipit.emit('env-ready');
  });

  shipit.blTask('build-next-app', async () => {
    await shipit.remote(
      `cd ${shipit.releasePath} && npm install --production && npm run build`
    );
    shipit.emit('built');
  });

  shipit.blTask('deploy-new-release', async () => {
    await shipit.remote(`pm2 delete -s ${appName} || :`);
    await shipit.remote(
      `pm2 start ${ecosystemFilePath} --env production --watch true`
    );
  });
};
/*
npm install && npm run build && pm2 delete -s hamoonja-front
pm2 start /home/easyadmin/code/js/hamoonja-front/ecosystem.config.js --env production --watch true
*/
