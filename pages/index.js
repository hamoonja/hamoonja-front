import { Container } from 'reactstrap';
import TestCallSuggestion from '../components/test-call-suggestion';
import ValueProposition from '../components/value-proposition';
import CallToAction from '../components/call-to-action';
import PaymentBox from '../components/payment-box';
const betaVersionText = `
(نسخه بتا)
`;

const Home = () => {
  return (
    <Container className='d-flex justify-content-center flex-column'>
      <p
        className='small-text'
        style={{ color: 'var(--sharp)', marginRight: 50 }}
      >
        {betaVersionText}
      </p>
      <ValueProposition />
      <TestCallSuggestion />
      <CallToAction />
      <PaymentBox />
      <p className='small-text'>
        <br />
        لطفا با پر کردن فرم نظرسنجی زیر به ما کمک کنید:
        <br />
        <br />
        <a
          href='https://docs.google.com/forms/d/e/1FAIpQLSf8HwrCt9Jy9ePMbTojjVv1KZtaMycW4KZ_ZPZcc1TBrthBgw/viewform'
          target='_blank'
        >
          فرم نظرسنجی
        </a>
      </p>
    </Container>
  );
};

export default React.memo(Home);
