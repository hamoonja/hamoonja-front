import App from 'next/app';
import DefaultHead from '../components/default-head';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../global-styles.css';

const MyApp = ({ Component, pageProps }) => {
  return (
    <>
      <DefaultHead />
      <Component {...pageProps} />
    </>
  );
};

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext);
  return appProps;
};

export default MyApp;
