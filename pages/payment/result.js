import { useRouter } from 'next/router';
import Link from 'next/link';
import { Container } from 'reactstrap';
import { convertNumbersToPersian } from '../../utils/strings';
import MeetingRoomBox from '../../components/meeting-room-box';

const errorText = `
پرداخت با مشکلی مواجه شد.
`;

const errorSupportText = `
اگر خودتان پرداخت را لغو نکردید یا پولی از حساب‌تان کم‌شده نگران نباشید.

ما همیشه آماده‌ایم تا مشکل‌تان را پی‌گیری کنیم.

به پست الکترونیک زیر پیغام بدهید:
`;

const backText = `
برگشت به صفحه اول
`;

const renderErrorMessage = () => (
  <>
    <Container>
      <p className='error'>{errorText}</p>
      <p>{errorSupportText}</p>
      <a href='mailto:support@hamoonja.com'>support@hamoonja.com</a>
    </Container>
    <Link href='/'>
      <a className='small-link mt-auto'>{backText}</a>
    </Link>
  </>
);

const PaymentResult = () => {
  const router = useRouter();
  const {
    query: { status, paymentRef, url },
  } = router;

  if (status !== 'OK') return renderErrorMessage();

  return (
    <>
      <p>پرداخت با موفقیت انجام شد.</p>
      <p>کد پی‌گیری پرداخت:</p>
      <p>{convertNumbersToPersian(paymentRef)}</p>
      <MeetingRoomBox url={url} />
      <Link href='/'>
        <a className='small-link'>{backText}</a>
      </Link>
    </>
  );
};
export default PaymentResult;
