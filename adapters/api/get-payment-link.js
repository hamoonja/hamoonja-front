import config from '../../config';

const getPaymentLink = async ({
  capacity,
  expDays,
  mobileNumber,
  amount,
  meetingTitle,
}) => {
  const paymentInfo = await fetch(
    `${
      config.apiBaseRoute
    }/payments?capacity=${capacity}&expDays=${expDays}&mobileNumber=${mobileNumber}&amount=${amount}&meetingTitle=${encodeURIComponent(
      meetingTitle
    )}`,
    {
      method: 'GET',
      headers: { Accept: 'application/json' },
    }
  );
  return paymentInfo.json();
};

export default getPaymentLink;
