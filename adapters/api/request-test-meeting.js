import config from './../../config';

const requestTestMeeting = async () => {
  try {
    const meetingInfo = await fetch(`${config.apiBaseRoute}/meetings`, {
      method: 'POST',
      body: JSON.stringify({ is_test: true }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return meetingInfo.json();
  } catch (error) {
    // TODO: handle errors
    console.log(error);
    throw error;
  }
};

export default requestTestMeeting;
