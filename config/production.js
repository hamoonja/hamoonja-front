export default {
  hostName: 'hamoonja.com',
  protocol: 'https',
  cookieOptions: { path: '/', domain: '.hamoonja.com' },
  apiBaseRoute: 'https://api.hamoonja.com',
};
