import defaultConfig from './default';
import productionConfig from './production';

const config =
  process.env.NODE_ENV === 'production' ? productionConfig : defaultConfig;

export const baseUrl = `${config.protocol}://${config.hostName}`;

export default config;
