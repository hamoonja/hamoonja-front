export default {
  hostName: 'localhost:3000',
  protocol: 'http',
  cookieOptions: { path: '/' },
  // apiBaseRoute: 'https://api.hamoonja.com',
  apiBaseRoute: 'http://localhost:4500',
};
