import { Input, FormGroup, Label, Row, Col } from 'reactstrap';
import {
  parseMobileNumber,
  convertNumbersToPersian,
} from './../../utils/strings';
import styles from './mobile-number-input.module.css';

const mobileNumberText = `
شماره موبایل:
`;

const placeholder = `
مثل ۰۹۱۲۳۴۵۶۷۸
`;

const MobileNumberInput = ({ onChange, ...rest }) => {
  const [value, setValue] = React.useState('');
  const [mobileNumber, setMobileNumber] = React.useState(null);
  const [isReady, setIsReady] = React.useState(false);

  const handleChange = React.useCallback(
    (e) => {
      const text = e.target.value;
      const { roman, mobile } = parseMobileNumber(text);
      if (mobile) {
        setIsReady(true);
        setValue(convertNumbersToPersian(mobile));
        setMobileNumber(mobile);
        onChange(mobile);
      } else if (!isNaN(roman)) {
        setIsReady(false);
        setValue(convertNumbersToPersian(roman));
        setMobileNumber(null);
        onChange(null);
      }
    },
    [value]
  );
  return (
    <Row className={styles.formRow}>
      <Col xs='6' sm='3' className={styles.label}>
        <Label for='mobile-number-input'>{mobileNumberText}</Label>
      </Col>
      <Col xs='12' sm='9'>
        <Input
          placeholder={placeholder}
          valid={isReady}
          value={value}
          id='mobile-number-input'
          onChange={handleChange}
          {...rest}
        />
      </Col>
    </Row>
  );
};

export default React.memo(MobileNumberInput);
