import { Row, Col, Label } from 'reactstrap';
import QuantityPicker from './../quantity-picker';

import styles from './quantity-picker-input.module.css';

const QuantityPickerInput = ({ label, id, ...rest }) => {
  return (
    <Row className={styles.formRow}>
      <Col xs='6' sm='3' className={styles.label}>
        <Label for={id}>{label}</Label>
      </Col>
      <Col xs='12' sm='9'>
        <QuantityPicker id={id} {...rest} />
      </Col>
    </Row>
  );
};

export default QuantityPickerInput;
