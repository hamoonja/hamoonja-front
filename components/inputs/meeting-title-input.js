import { Row, Col, Input, Label } from 'reactstrap';
import styles from './meeting-title-input.module.css';

const placeholder = `عنوان اتاق جلسه (مثلا کلاس فیزیک ۳)`;

const meetingTitleText = `عنوان جلسه:`;

const MeetingTitleInput = ({ value, onUpdate }) => (
  <Row className={styles.formRow}>
    <Col xs='6' sm='3' className={styles.label}>
      <Label for='meeting-title-input'>{meetingTitleText}</Label>
    </Col>
    <Col xs='12' sm='9'>
      <Input
        placeholder={placeholder}
        id='meeting-title-input'
        value={value}
        onChange={(e) => onUpdate(e.target.value)}
      />
    </Col>
  </Row>
);

export default MeetingTitleInput;
