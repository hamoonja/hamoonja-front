import Head from 'next/head';

const siteName = 'همون‌جا';
const title = 'همون‌جا | تماس تصویری سریع، امن و آسان';
const description =
  'به راحتی یک کلیک کردن یک تماس تصویری شروع کنید و لینک آن را برای هر کسی که می‌خواهید بفرستید. برای شرکت در جلسه  احتیاج به نصب هیچ نرم‌افزاری نیست.';

const DefaultHead = () => (
  <Head>
    <title key='title'>{title}</title>

    <link rel='apple-touch-icon' sizes='57x57' href='/apple-icon-57x57.png' />
    <link rel='apple-touch-icon' sizes='60x60' href='/apple-icon-60x60.png' />
    <link rel='apple-touch-icon' sizes='72x72' href='/apple-icon-72x72.png' />
    <link rel='apple-touch-icon' sizes='76x76' href='/apple-icon-76x76.png' />
    <link
      rel='apple-touch-icon'
      sizes='114x114'
      href='/apple-icon-114x114.png'
    />
    <link
      rel='apple-touch-icon'
      sizes='120x120'
      href='/apple-icon-120x120.png'
    />
    <link
      rel='apple-touch-icon'
      sizes='144x144'
      href='/apple-icon-144x144.png'
    />
    <link
      rel='apple-touch-icon'
      sizes='152x152'
      href='/apple-icon-152x152.png'
    />
    <link
      rel='apple-touch-icon'
      sizes='180x180'
      href='/apple-icon-180x180.png'
    />
    <link
      rel='icon'
      type='image/png'
      sizes='192x192'
      href='/android-icon-192x192.png'
    />
    <link rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png' />
    <link rel='icon' type='image/png' sizes='96x96' href='/favicon-96x96.png' />
    <link rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png' />
    <link rel='manifest' href='/manifest.json' />
    <meta name='msapplication-TileColor' content='#b2dbd5' />
    <meta name='msapplication-TileImage' content='/ms-icon-144x144.png' />
    <meta name='theme-color' content='#b2dbd5'></meta>

    <meta key='description' name='description' content={description} />
    <meta key='og-url' property='og:url' content='https://hamoonja.com' />
    <meta key='og-type' property='og:type' content='website' />
    <meta key='og-title' property='og:title' content={title} />
    <meta
      key='og-description'
      property='og:description'
      content={description}
    />
    <meta key='og-site_name' property='og:site_name' content={siteName} />
  </Head>
);

export default React.memo(DefaultHead);
