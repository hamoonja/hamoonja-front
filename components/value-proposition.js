import styles from './value-proposition.module.css';

const valuePropositionText1 = `
تماس تصویری ساده و باکیفیت
`;
const valuePropositionText2 = `
برای کسب‌و‌کار شما
`;

const ValueProposition = () => (
  <div className={styles.container}>
    <h5 className={styles.titr}>
      {valuePropositionText1}
      <br />
      {valuePropositionText2}
    </h5>
  </div>
);

export default ValueProposition;
