import { Button } from 'reactstrap';

import styles from './meeting-room-box.module.css';

const readyText = `
اتاق جلسه‌ی شما آماده است:
`;

const enterRoomText = `
ورود به اتاق جلسه
`;

const copyButtonText = `
کپی کردن آدرس
`;

const renderAlertText = (url) => `
آدرس زیر در کلیپبورد کپی شد:

${url}

لطفا آن را در جای مطمئنی نگه دارید.
`;

const MeetingRoomBox = ({ url }) => {
  const copyToClipboard = React.useCallback(() => {
    const targetElem = document.getElementById('meeting-room-url');
    targetElem.select();
    targetElem.setSelectionRange(0, 99999);
    document.execCommand('copy');
    alert(renderAlertText(targetElem.value));
  }, []);
  return (
    <div className={styles.container}>
      <p>{readyText}</p>
      <input
        value={url}
        onChange={() => ({})}
        type='text'
        id='meeting-room-url'
      />
      <Button
        color='link'
        className={styles.copyButton}
        onClick={copyToClipboard}
      >
        {copyButtonText}
      </Button>
      <a href={url} className={styles.button}>
        {enterRoomText}
      </a>
    </div>
  );
};

export default MeetingRoomBox;
