const AsyncButton = ({ children, onClick, ...rest }) => {
  const [waiting, setWaiting] = React.useState(false);
  const [error, setError] = React.useState(null);

  const handleClick = React.useCallback(async (e) => {
    try {
      setWaiting(true);
      await onClick(e);
    } catch (err) {
      // TODO: handle this error later
      console.log(err);
    } finally {
      setWaiting(false);
    }
  }, []);

  return (
    <button
      type='button'
      onClick={handleClick}
      disabled={waiting}
      color={waiting ? 'secondary' : undefined}
      {...rest}
    >
      {children}
    </button>
  );
};

export default AsyncButton;
