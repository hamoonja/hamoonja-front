import styles from './quantity-picker.module.css';
import { convertNumbersToPersian } from './../utils/strings';

const QuantityPicker = ({
  defaultValue = 10,
  max = 20,
  min = 0,
  unit = '',
  onChange = () => ({}),
  ...otherProps
}) => {
  const [value, setValue] = React.useState(defaultValue);
  const increment = React.useCallback(() => {
    if (value + 1 <= max) {
      setValue((oldValue) => oldValue + 1);
      onChange(value + 1);
    }
  }, [value]);

  const decrement = React.useCallback(() => {
    if (value - 1 >= min) {
      setValue((oldValue) => oldValue - 1);
      onChange(value - 1);
    }
  }, [value]);

  return (
    <div className={styles.container}>
      <input
        type='button'
        value='+'
        className={styles.button}
        onClick={increment}
      />

      <input
        className={styles.quantity}
        name='quantity'
        value={`${convertNumbersToPersian(value)} ${unit}`}
        title='Qty'
        disabled
        {...otherProps}
      />
      <input
        type='button'
        value='-'
        className={styles.button}
        onClick={decrement}
      />
    </div>
  );
};

export default QuantityPicker;
