const buyText = 'همین حالا یک اتاق جلسه برای خودتان بسازید:';

const CallToAction = () => (
  <div
    className='d-flex flex-column align-items-center'
    style={{ color: 'var(--main-dark)' }}
  >
    <h5 style={{ marginTop: 30, marginBottom: 30 }}>و</h5>
    <p style={{ fontSize: '0.9rem', marginBottom: 60 }}>{buyText}</p>
  </div>
);

export default CallToAction;
