import Link from 'next/link';
import { requestTestMeeting } from '../adapters/api';
import AsyncButton from './async-button';

import styles from './test-call-suggestion.module.css';

const buttonText = 'یک جلسه‌ی آزمایشی برگزار کنید';
const testCallAlert = 'طول جلسه‌ی آزمایشی حداکثر ۵ دقیقه است.';

const TestCallSuggestion = () => {
  const openTestCall = React.useCallback(async () => {
    const testMeeting = await requestTestMeeting();
    window.location.replace(testMeeting.url);
  }, []);
  return (
    <div className={styles.container}>
      <AsyncButton className={styles.button} onClick={openTestCall}>
        {buttonText}
      </AsyncButton>
      <p className={styles.alert}>{testCallAlert}</p>
    </div>
  );
};

export default React.memo(TestCallSuggestion);
