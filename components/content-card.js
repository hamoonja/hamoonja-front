import styles from './content-card.module.css';

const ContentCard = ({ children, className = '', ...otherProps }) => (
  <div className={`${styles.cardContainer} ${className}`} {...otherProps}>
    {children}
  </div>
);

export default React.memo(ContentCard);
