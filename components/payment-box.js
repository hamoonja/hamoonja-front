import { Form } from 'reactstrap';
import { buildPriceString, calculatePrice } from '../utils/price';
import MobileNumberInput from './inputs/mobile-number-input';
import AsyncButton from './async-button';
import QuantityPickerInput from './inputs/quantity-picker-input';
import getPaymentLink from '../adapters/api/get-payment-link';

import styles from './payment-box.module.css';
import MeetingTitleInput from './inputs/meeting-title-input';

const capacityText = `
گنجایش اتاق:
`;

const validityText = `
مدت اجاره:
`;

const disabledText = `

برای پرداخت شماره موبایل خود را وارد کنید.
`;

const payButtonText = 'پرداخت';

const discountText = `
به دلیل آغاز به کار سرویس قیمت‌ها با تخفیف ویژه محاسبه شده‌اند.
`;

const MIN_CAPACITY = 2;
const MAX_CAPACITY = 40;

const MIN_EXP_DAYS = 1;
const MAX_EXP_DAYS = 90;

const renderPrice = (capacity, expDays) => (
  <div className={styles.priceTag}>
    <div>{`قیمت: `}</div>
    <div className={styles.price}>{buildPriceString(capacity, expDays)}</div>
    <div>{` تومان`}</div>
  </div>
);

const PaymentBox = () => {
  const [capacity, setCapacity] = React.useState(10);
  const [expDays, setExpDays] = React.useState(30);
  const [mobileNumber, setMobileNumber] = React.useState(null);
  const [meetingTitle, setMeetingTitle] = React.useState(null);
  const [errorMessage, setErrorMessage] = React.useState(null);

  const handleSubmit = React.useCallback(
    async (e) => {
      e.preventDefault();
      try {
        setErrorMessage(null);
        const { url } = await getPaymentLink({
          capacity,
          expDays,
          mobileNumber,
          meetingTitle,
          amount: calculatePrice(capacity, expDays),
        });
        if (!url) throw new Error('something went wrong');
        window.location.replace(url);
      } catch (error) {
        console.error(error);
        setErrorMessage(error.message);
      }
    },
    [capacity, expDays, mobileNumber, meetingTitle]
  );
  return (
    <Form className={styles.container} onSubmit={handleSubmit}>
      <QuantityPickerInput
        label={capacityText}
        min={MIN_CAPACITY}
        max={MAX_CAPACITY}
        unit='نفر'
        defaultValue={10}
        onChange={setCapacity}
      />
      <QuantityPickerInput
        label={validityText}
        min={MIN_EXP_DAYS}
        max={MAX_EXP_DAYS}
        unit='روز'
        defaultValue={30}
        onChange={setExpDays}
      />
      <MobileNumberInput required onChange={setMobileNumber} />
      <MeetingTitleInput value={meetingTitle} onUpdate={setMeetingTitle} />
      {renderPrice(capacity, expDays)}
      <p className='small-text'>{discountText}</p>

      <button type='submit' className={styles.button} disabled={!mobileNumber}>
        {payButtonText}
      </button>
      {errorMessage && <div className={styles.error}>{errorMessage}</div>}
      {!mobileNumber && <p className={styles.disabledText}>{disabledText}</p>}
    </Form>
  );
};

export default React.memo(PaymentBox);
